[![Foo](https://farm7.staticflickr.com/6166/6204326415_11ac11b3e9.jpg)](http://thenocklist.com/brands/jasonjenkinsposters.html)

1. FORK THE [Jason Jenkins Posters WEBSITE](https://bitbucket.org/nocklist/jasonjenkinsposters/fork)

2. REGISTER WITH [ZAZZLE](http://www.zazzle.com/)

3. READ THE [ZAZZLE ASSOCIATES SECTION](http://www.zazzle.com/my/associate/associate)  
HERE IS WHERE YOU WILL FIND YOUR ASSOCIATE ID.  YOU WILL NEED THIS LATER FOR CREATING LINKS.

4. READ THE [ZAZZLE How to Build Referral Links](http://www.zazzle.com/sell/affiliates/referrallinks)

5. EDIT THE [jasonjenkinsposters.html] FILE.  
PLACE YOUR ZAZZLE ID IN THE APPROPRIATE LINKS.